import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { BehaviorSubject } from 'rxjs';

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetail[];
}

function uppercaseAllAuthorNames(videos: Video[]) {
  return videos.map(
    v => ( { ...v, author: v.author.toUpperCase() } )
  );
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  private _currentVideo: Video;

  currentVideo = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  setCurrentVideo(video: Video) {
    this._currentVideo = video;
    this.currentVideo.next(this._currentVideo);
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        map(uppercaseAllAuthorNames),
        tap(videos => {
          if (videos && videos[0]) {
            this.setCurrentVideo(videos[0]);
          }
        })
      );
  }
}
