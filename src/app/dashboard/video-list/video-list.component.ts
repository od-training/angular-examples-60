import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];

  constructor(private svc: VideoDataService) { }

  ngOnInit() {
  }
}
