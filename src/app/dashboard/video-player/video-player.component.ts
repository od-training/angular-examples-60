import { Component, Input, OnInit } from '@angular/core';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  
  constructor(private svc: VideoDataService) { }

  ngOnInit() {
  }

}
