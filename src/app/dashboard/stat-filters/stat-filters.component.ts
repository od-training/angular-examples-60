import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {

  fg: FormGroup;
  
  constructor(fb: FormBuilder) {

    this.fg = fb.group({
      author: ['Sani Yusuf'],
      dateFrom: [''],
      dateTo: ['']
    });
  }

  ngOnInit() {
  }

}
