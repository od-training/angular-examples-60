import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Video, VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  videoData$: Observable<Video[]> | undefined;

  constructor(private svc: VideoDataService) { }

  ngOnInit() {
    this.videoData$ = this.svc.loadVideos();
  }

}
